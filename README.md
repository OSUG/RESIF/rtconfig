#  Génération automatique des configurations temps réel

Cet outil permet de générer la configuration d'agents de collecte des données temps réel, avec le contrôleur [https://supervisord.org](supervisord).

## Installation

    pip install [--user]  rtconfig --index-url https://gricad-gitlab.univ-grenoble-alpes.fr/api/v4/projects/8398/packages/pypi/simple

## Utilisation

L'outil peut être appelé en deux saveurs différentes :

* rtserver: Pour une configuration adaptée au serveur temps réel (par exemple [notre serveur resif-rts](https://wiki.osug.fr/!isterre-geodata/resif/systemes/services/flux_temps_reel) )
* budcollector: Pour générer une configuration d'un serveur de collecte sur un espace bud (par exemple [resif-budcollector](https://wiki.osug.fr/!isterre-geodata/resif/systemes/services/flux_temps_reel))

La commande la plus importante :

  rtconfig --help


## Principe de fonctionnement

### Description des entités

Les flux temps réels sont représentés par la classe `Stream`. La classe a la capacité d'analyser une représentation d'un canal en différents formats (séparés par des `.`, par des `_` ou par des espaces). Cette classe a aussi la propriété d'être triable.

Les réseaux sismologiques sont représentés par la classe `Network`. Celle-ci contient une liste de Streams, le code réseau, la politique de restriction.

Les nœuds A temps réels sont représentés par la classe `Node`. Celle-ci est simplement constituée d'un nom de serveur (ou son IP) et d'un dictionnaire de `Networks`.
La classe nœud est capable de découvrir tous les réseaux diffusés par un nœud. Cette opération est réalisée à l'initialisation d'une instance de classe.
La fonction `Node.add_stream()` permet d'ajouter un stream dans le réseau d'un node. Cette opération ne se fait que si le réseau est connu du node.
La classe `Node` a la capacité de générer des instances de filtres à partir de ses attributs.

L'inventaire des canaux, regroupés par réseaux est réalisé par la classe `Metadata`. Son instance unique est exposée avec `metadata.invenrory` (ce qui se rapproche le plus d'un singleton). Outre les canaux tels que définis par le webservice station, l'inventaire construit aussi une liste de règles permettant de décrire la logique d'affectation d'un stream à un nœud.

Les filtres représentent la liste des streams qui doivent être collectés par un client `slarchive`. Ils ont vocation à être écrits dans des fichiers. On peut ajouter des streams à un filtre soit un par un, soit en ouvrant le fichier de sélection.

Le programme se déroule comme suit:
* chargement des règles (logique d'affectation d'un stream à un node) à partir d'un fichier yaml
* collecte de la métadonnée (réalisé une seule fois lors de l'instanciation du premier node `rtconfig.metadata.invenrory`)
* pour chaque réseau de l'inventaire correspondant à la politique de restriction indiquée en ligne de commande
  * pour chaque stream, détermination du bon node et ajout de ce stream au node
* création des instances de filtres (fonction `rtconfig.node.make_filters()`)
* écriture des configurations de filtres et de supervisord (fonction `rtconfig.utils.write_config()`)
* redémarrage des process si nécessaire, avec supervisord (fin de `rtconfig.cli.rtserver()`)


### Politique de collecte pour un node

Pour que le programme fonctionne, il faut lui donner une liste de règles pour l'affectation d'un stream à un node.

Pour cela, on se base sur deux expressions régulières :
* sur le code réseau
* sur le channel

De plus, selon que l'outil est lancé avec l'option `--restricted-streams` ou pas, seuls les réseaux restreints (respectivement, ouvert) seront considérés.

Un exemple de fichier de règles, correspondant à rtserver:

``` yaml
rules:
  - networks_regex: (CL|MT)
    host: rtserve.u-strasbg.fr
  - networks_regex: (FR|ND|FO|RD)
    channels_regex: .N.
    host: bud-resif-a.u-ga.fr
  - networks_regex: (FR|ND|FO|RD)
    channels_regex: .[^N].
    host: rtserve.u-strasbg.fr
  - networks_regex: (RA|[XYZ0-9][A-Z0-9])
    host: bud-resif-a.u-ga.fr
  - networks_regex: RD
    channels_regex: .[^N].
    host: 132.167.240.22
  - networks_regex: (G|GL|PF|MQ|QM|WI)
    host: rtserver.ipgp.fr
```

Un autre exemple pour budcollector (streams publics):

``` yaml
rules:
  - networks_regex: .+ 
    host: rtserve.resif.fr
```

Et enfin, un autre exemple pour les streams restreints:

``` yaml
rules:
  - networks_regex: .+ 
    host: rtserve.resif.fr:18001
```

### Exemple d'utilisation

    rtconfig --supervisor-conf ~/tmp/config/slclient_processes.ini \
             --slclients-confdir ~/tmp/config \
             --archivedir ~/tmp/data \
             --clean  \
             --rules-file ~/tmp/rtconfig-rtserver.yaml \
             --mode rtserver

Cette commande écrira un fichier supervisor `~/tmp/config/slclient_processes.ini`, les configuration slarchive dans `~/tmp/config`, les données dans `~/tmp/data`.
Les fichiers de configurations inutilisés seront nettoyés (`--clean`)
Le fichier de règle yam sera chargé à partir du chemin `~/tmp/rtconfig-rtserver.yaml`
Et le mode de fonctionnement sera pour rtserver.

