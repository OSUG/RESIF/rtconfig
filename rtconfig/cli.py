#!/usr/bin/env python3
import logging
import click
import click_log
import sys
from pathlib import Path
from rtconfig import metadata
from rtconfig.node import Node
from rtconfig.utils import write_config, write_supervisord_conf

logger = logging.getLogger(__name__)
click_log.basic_config(logger)

@click.command()
@click.option("--supervisor-conf",
              help="Supervisord configuration file. This file will be completely overwritten ! Environment variable: RTCONFIG_SUPERVISOR_CONF",
              default=Path('~').expanduser().joinpath('supervisor/conf.d/slclients.ini'),
              show_default=True,
              envvar="RTCONFIG_SUPERVISOR_CONFDIR",
              type = click.Path(dir_okay = False, file_okay = True, writable = True, path_type=Path))
@click.option("--slclients-confdir",
              help="Configuration directory for filters and state files for slarchive. Environment variable: RTCONFIG_SLCLIENTS_CONFDIR",
              default=Path('~').expanduser().joinpath('config/slclients'),
              show_default=True,
              envvar="RTCONFIG_SLCLIENTS_CONFDIR",
              type = click.Path(dir_okay = True, file_okay = False, exists = True, writable = True, path_type=Path))
@click.option("--archivedir",
              help="Archive directory where the data will be written.",
              default=Path('~').expanduser().joinpath('work'),
              show_default=True,
              envvar="RTCONFIG_ARCHIVEDIR",
              type = click.Path(dir_okay = True, file_okay = False, writable = True, path_type=Path))
@click.option("--max-streams",
              help="Maximum number of streams for a slclient instance. Environment variable: RTCONFIG_MAX_STREAMS",
              default=100,
              show_default=True,
              envvar="RTCONFIG_MAX_STREAMS",
              type = int)
@click.option("--clean",
              help="Remove unused configuration files.",
              is_flag=True,
              show_default=True,
              default=False)
@click.option("--keep",
              help="Keep streams that disapeared from a node.",
              is_flag=True,
              show_default=True,
              default=True)
@click.option("--restart-processes",
              help="Restart all supervisor managed processes if needed",
              is_flag=True,
              show_default=True,
              default=False)
@click.option("--rules-file",
              help="Path to a file containing the nodes rules",
              default=Path('~').expanduser().joinpath('config').joinpath('rtconfig.yaml'),
              show_default=True,
              type = click.File(mode='r'))
@click.option("--restricted-streams",
              is_flag=True,
              show_default=True,
              default=False,
              help="Take only restricted streams")
@click.option("--strict/--no-strict",
              is_flag=True,
              show_default=True,
              default=True,
              help="In strict mode (default), any failure will cause the program to exit.")
@click.option("--mode",
              help="Build configuration profile. This will change the slarchive options.",
              type=click.Choice(['rtserver', 'budcollector']))
@click_log.simple_verbosity_option(logger)
def cli(supervisor_conf,
        slclients_confdir,
        max_streams,
        archivedir,
        clean,
        keep,
        restart_processes,
        rules_file,
        restricted_streams,
        mode,
        strict):

    try:
        logger.info("Loading rules from %s", rules_file.name)
        metadata.inventory.load_rules(rules_file)
    except Exception as err:
        logger.error("Unable to load rules from file %s", rules_file)
        logger.error(err)
        sys.exit(1)

    logger.info("Assigning all streams from inventory to nodes")
    nodes = {}
    unresponsive_nodes = [] # A list of unresponsive nodes
    for _,net in metadata.inventory.networks.items():
        logger.info("   network %s", net.code)
        if restricted_streams == net.restricted:
            for stream in net.streams:
                host = metadata.inventory.get_node(stream.net, stream.cha)
                if host not in nodes.keys() and host not in unresponsive_nodes:
                    try:
                        nodes[host] = Node(host=host)
                        nodes[host].add_stream(stream)
                    except RuntimeError as e:
                        logger.error(e)
                        logger.info("Node %s is not responding. Skip it", host)
                        unresponsive_nodes.append(host)
                        if strict:
                            sys.exit(1)
        else:
            logger.debug("    ignoring due to restriction policy mismatch")

    logger.debug("Nodes set up")

    filters= []
    for _,rts in nodes.items():
        logger.info("Building filters files for %s", rts.host)
        filters += rts.make_filters(slclients_confdir=slclients_confdir,
                                    max_streams=max_streams)

    logger.info("Writing supervisord configuration")
    write_supervisord_conf(filters,
                           mode=mode,
                           archivedir=archivedir,
                           confpath=str(supervisor_conf))
    logger.info("Writing selectors files")
    write_config(filters,
                 restart_processes=restart_processes,
                 keep_disapeared_streams=keep,
                 clean = clean)


if __name__ == "__main__":
    cli()
