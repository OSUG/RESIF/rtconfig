#!/usr/bin/env python3
from pathlib import Path
from typing import List
from rtconfig.network import Network
from rtconfig.streams import Stream
from dataclasses import dataclass, field
import logging

logger = logging.getLogger()

@dataclass
class Filter(Network):
    """
    A class to represent filters for slclients.
    """
    rtserver: str = ""
    chunk: int = 0
    read_from_file: bool = False
    confdir: str = "."
    selections: List[str] = field(default_factory=list)

    def __post_init__(self):
        if self.read_from_file:
            self._streams_from_file()

    def slclient_id(self):
        """
        A way to format a unique slclient id
        """
        return f"{self.rtserver.replace('-','_').replace('.','_').replace(':','_')}__{self.code}__{self.chunk:02d}"

    def filename(self):
        return Path(self.confdir) / Path(self.slclient_id())


    def _streams_from_file(self):
        logger.debug("Open file %s", self.filename())
        try:
            with open(self.filename(), 'r') as fp:
                self.selections = fp.read().splitlines()
        except FileNotFoundError:
            # Si le fichier n'existe pas, pas de panique, il sera créé
            logger.info("Stream file %s does not exist yet.",self.filename())
        self.selections.sort()

    def add_stream(self, stream: Stream):
        """
        Add a stream to the filter.
        Ignore if already registered.
        """
        if self.format_stream(stream) not in self.selections:
            self.selections.append(self.format_stream(stream))
            self.selections.sort()


    def format_stream(self, stream: Stream) -> str:
        # Crée une chaine de caractère au format filtres
        return f"{stream.net} {stream.sta} {stream.loc}{stream.cha}"

    def __eq__(self, __value) -> bool:
        if isinstance(__value, Filter):
            return self.selections == __value.selections
        else:
            return False

    def write_file(self):
        """
        Écrit la liste des filtres dans le fichier self.filename
        """
        with open(self.filename(), "w") as fp:
            logger.info("Writing %s selections in %s", len(self.selections), self.filename())
            fp.write("\n".join(self.selections)+"\n")
