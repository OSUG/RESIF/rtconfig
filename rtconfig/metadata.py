#!/usr/bin/env python3
from dataclasses import dataclass, field
from datetime import date
import logging
import re
import yaml
from typing import Dict, List
import xml.etree.ElementTree as ET
import requests
from rtconfig.network import Network
from rtconfig.rule import RoutingRule
from rtconfig.streams import Stream
from functools import lru_cache

logger = logging.getLogger()

@dataclass
class Metadata():
    """
    Class to make an inventory out of the FDSN station webservice.
    This can be used as a Global Object:
      import rtconfig.metadata as metadata
    Then the global inventory (instance of class Metadata is available as metadata.inventory
    """
    xml: str = ""
    networks: Dict[str,Network] = field(default_factory=dict)
    rules: List[RoutingRule] = field(default_factory=list)

    def __post_init__(self):
        self.get_active_streams()

    def __hash__(self):
        return hash(self.xml)

    def get_active_streams(self):
        # Si on n'a pas encore le XML, on le récupère et on construit la liste des réseaux
        if self.xml == "":
            self._update()
            self._make_networks_metadata()
        streams = []
        for net in self.networks:
            streams += self.networks[net].streams
        return streams

    def get_restriction(self, net : str) -> bool:
        if self.xml == "":
            self._update()
            self._make_networks_metadata()
        return self.networks[net].restricted

    def has_stream(self, stream : Stream) -> bool:
        """
        Teste si un stream fait partie de l'inventaire
        """
        if self.xml == "":
            self._update()
            self._make_networks_metadata()
        for _, net in self.networks.items():
            if stream in net.streams:
                return True
        return False

    @lru_cache
    def get_node(self, net:str, cha:str)-> str:
        """
        À partir d'un code réseau et d'un channel, trouve le serveur temps réel correspondant.
        """
        for r in self.rules:
            if r.validate(net, cha):
                return r.host
        logger.warning("No rule to determine where to get stream %s_*_*_%s from.", net, cha)


    def _make_networks_metadata(self):
        """
        Fabrique la métadonnée organisée par code réseau
        Suppose que self.xml est bien fait.
        """
        ns = {'stationxml': 'http://www.fdsn.org/xml/station/1'}
        root = ET.fromstring(self.xml)
        for net in root.findall('.//stationxml:Network', ns):
            if net not in self.networks.keys():
                self.networks[net.attrib['code']] = Network(net.attrib['code'])
                logger.info("Adding network %s", net.attrib['code'])
            logger.debug("Entering FDSN:%s, %s", net.attrib['code'], net.attrib['restrictedStatus'])
            self.networks[net.attrib['code']].restricted = net.attrib['restrictedStatus'] != 'open'
            for sta in net.findall('./stationxml:Station', ns):
                logger.debug("  Entering FDSN:%s_%s", net.attrib['code'], sta.attrib['code'])
                for cha in sta.findall('./stationxml:Channel', ns):
                    logger.debug("    Entering FDSN:%s_%s_%s_%s",
                                 net.attrib['code'],
                                 sta.attrib['code'],
                                 cha.attrib['locationCode'],
                                 cha.attrib['code'])
                    self.networks[net.attrib['code']].streams.append(Stream(net.attrib['code'],
                                                                            sta.attrib['code'],
                                                                            cha.attrib['locationCode'],
                                                                            cha.attrib['code']))

    def _update(self):
        """
        Récupère la métadonnée au format XML
        """
        today = date.today()
        try:
            stationxml = requests.get(f"https://ws.resif.fr/fdsnws/station/1/query?refreshcache&level=channel&endafter={today.strftime('%Y-%m-%d')}&format=xml")
        except Exception as err:
            raise Exception("Error making request")
        self.xml = stationxml.text

    def load_rules(self, conf:str):
        """
        Charge les règles décrites en yaml et en fait une structure de donnée
        params: conf is a string containing a valid yaml config
        """
        # open config
        # load rules section as is.
        # Maybe a Rule object would be nice
        try:
            logger.debug("Opening %s", conf)
            conf_dict = yaml.safe_load(conf)
            for r in conf_dict['rules']:
                if "channels_regex" in r.keys():
                    channels_regex = r["channels_regex"]
                else:
                    channels_regex = r"..."
                new_rule = RoutingRule(host=r['host'], networks_regex=r['networks_regex'], channels_regex=channels_regex)
                self.rules.append(new_rule)
        except Exception as err:
            raise err




    def wich_host(self, stream:Stream) -> str:
        """
        À partir du stream passé en paramètre, détermine quel serveur temps réel est sensé le distribuer.
        """
        for r in self.rules:
            if re.match(r['networks_regex'], stream.net):
                if 'channel_regex' in r.keys():
                    if re.match(r['channel_regex'], stream.cha):
                        return r['node']
                    else:
                        continue
                return r['node']
        logger.error("No rule matched stream %s", stream)
        raise ValueError(f"No rule to match {stream}")



# A global object for the programs using this class
try:
    inventory
except NameError:
    logger.info("Collecting metadata in inventory")
    inventory = Metadata()
else:
    logger.info("Metadata already defined")
