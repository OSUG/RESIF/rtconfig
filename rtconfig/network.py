#!/usr/bin/env python3
import logging
from dataclasses import dataclass, field
from typing import List
from rtconfig.streams import Stream

logger = logging.getLogger()

@dataclass
class Network():
    code: str
    restricted: bool = True
    streams: List[Stream] = field(default_factory=list)
    exclude_accelero: bool = False

    def add_stream(self, stream: Stream) -> bool:
        """
        Add a stream to this network.
        Return true if stream is added, false if not.
        This method does not check any consistency with the rules.
        """
        logger.debug("[%s] Choosing if stream %s should be added to network",
                     self.code, stream)
        if stream.net != self.code:
            logger.debug("    %s does not belong to me", stream)
            return False
        if stream not in self.streams:
            logger.debug("    stream %s can be added.", stream)
            self.streams.append(stream)
        else:
            logger.debug("    %s is already in network", stream)
        return True
