#!/usr/bin/env python3

import subprocess
import logging
from dataclasses import dataclass, field
from typing import Dict, List
from rtconfig.filter import Filter
from rtconfig.network import Network
import rtconfig.metadata as metadata
from rtconfig.streams import Stream

logger = logging.getLogger()

@dataclass
class Node():
    """
    A node refers to a data producer, the logic to select data streams from and the networks it distributes.
    """

    host: str
    networks: Dict[str,Network] = field(default_factory=dict)

    def __post_init__(self):
        """
        After initialisation, create streams
        """
        try:
          self._get_active_networks()
        except RuntimeError as e:
            raise e

    def _get_active_networks(self):
        """
        Collect networks from this server
        """
        logger.debug("Getting active streamse for %s", self.host)
        try:
            completed_process = subprocess.run(['slinktool', '-Q', self.host], capture_output=True, timeout=10)
            for l in completed_process.stdout.splitlines():
                line = l.decode("utf-8")
                stream = Stream()
                try:
                    stream.parse(nslc=line)
                except ValueError:
                    logger.info("Ignoring line %s", line)

                self.add_network(stream.net)

        except subprocess.TimeoutExpired as err:
            logger.error(err)
            raise RuntimeError(f"Node {self.host} unreachable")
        logger.info("Node %s publishes networks %s", self.host, self.networks.keys())

    def add_network(self, code: str):
        """
        Create the network dictionary entry for this stream if needed.
        """
        if code not in self.networks.keys():
            logger.debug("    network not yet defined for %s", self.host)
            if code in metadata.inventory.networks.keys():
                self.networks[code] = Network(code=code, restricted=metadata.inventory.networks[code].restricted,)
            else:
                logger.debug("    ignoring network %s not in inventory", code)

    def add_stream(self, stream: Stream):
        """
        Add a stream to this node only if the network is published by this node.
        """
        if stream.net not in self.networks.keys():
            logger.debug("    Network %s not defined for %s. Ignoring.", stream.net, self.host)
        else:
            self.networks[stream.net].add_stream(stream)

    def make_filters(self,
                     slclients_confdir:str,
                     max_streams:int) -> List[Filter]:
        """
        Construit des instances de Filter à partir des networks de ce node.
        slclients_confdir: le répertoire de config des clients slink
        max_streams: le nombre de streams max dans un filtre
        Renvoie une liste de filtres à partir des paramètres nécessaires:
        """
        filters = []
        for _,net in self.networks.items():
            if len(net.streams) == 0:
                continue
            logger.info("  Building filters list for %s", net.code)
            streams_count = 0
            chunk = 0
            slfilter = Filter(code=net.code,
                              rtserver=self.host,
                              chunk=chunk,
                              confdir=slclients_confdir,
                              restricted=net.restricted)
            for s in net.streams:
                slfilter.add_stream(s)
                streams_count += 1
                if streams_count >= max_streams:
                    filters.append(slfilter)
                    streams_count = 0
                    chunk += 1
                    del(slfilter)
                    slfilter = Filter(code=net.code,
                                      rtserver=self.host,
                                      chunk=chunk,
                                      confdir=slclients_confdir,
                                      restricted=net.restricted)
            filters.append(slfilter)
            logger.debug("  %s filters, %s streams", chunk+1, (chunk+1)*max_streams + streams_count)
        return filters

