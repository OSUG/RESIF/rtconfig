#!/usr/bin/env python3
from dataclasses import dataclass
from functools import lru_cache
import logging
import re

logger = logging.getLogger()

@dataclass
class RoutingRule():
    """
    A rule to determine where a stream has to be taken from
    """
    host: str
    networks_regex: str
    channels_regex: str

    def __post_init__(self):
        try:
            self._net_pattern = re.compile(self.networks_regex)
        except re.error as err:
            logger.error("Configure networks_regex %s is not a valid expression",self.networks_regex)
            raise ValueError(err.msg)
        try:
            self._cha_pattern = re.compile(self.channels_regex)
        except re.error as err:
            logger.error("Configured channel_regex %s is not a valid expression",self.networks_regex)
            raise ValueError(err.msg)

    @lru_cache
    def validate(self, net, cha ) -> bool:
        """
        Validate the stream against the rule
        """
        if re.match(self._net_pattern, net) and re.match(self._cha_pattern, cha):
            logger.debug("%s match %s and %s match %s for node %s ", net, self._net_pattern, cha, self._cha_pattern, self.host)
            return True
        return False

    def __hash__(self) -> int:
        return hash((self.networks_regex, self.channels_regex))
