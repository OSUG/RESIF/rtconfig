#!/usr/bin/env python3

from dataclasses import dataclass
import re

@dataclass
class Stream():
    """
    Modélise un flux de données (NSLC) avec sa politique de trstriction
    """
    net: str =""
    sta: str =""
    loc: str =""
    cha: str =""

    def parse(self, nslc: str):
        # On matche avec l'expression régulière adaptée pour capturer différentes formes de ligne décrivant un NSLC
        # https://regex101.com/r/UaCgIl/1
        try:
            matches = re.search(r"^([A-Z0-9]+)(\.|_|\s)+([A-Z0-9]+)\2+([A-Z0-9]+|)\2+([A-Z0-9]{3})", nslc)
        except Exception as err:
            raise err
        if matches is not None:
            self.net = matches.group(1)
            self.sta = matches.group(3)
            if all([ l == ' ' for l in matches.group(4)]):
                self.loc = ""
            else:
                self.loc = matches.group(4)
            self.cha = matches.group(5)
        else:
            raise ValueError(f"No matches for {nslc}")

    def __repr__(self) -> str:
        return f"{self.net}_{self.sta}_{self.loc}_{self.cha}"

    def __eq__(self, __value) -> bool:
        if isinstance(__value, Stream):
            return self.net == __value.net and self.sta == __value.sta and self.loc == __value.loc and self.cha == __value.cha
        else:
            return False

    def __lt__(self, __value) -> bool:
        return repr(self) < repr(__value)

    def __gt__(self, __value) -> bool:
        return repr(self) > repr(__value)

