#!/usr/bin/env python3

from typing import List
from pathlib import Path
import logging
import difflib
import subprocess
from rtconfig.filter import Filter

logger = logging.getLogger()

def write_config(filters: List[Filter],
                 restart_processes:bool = False,
                 keep_disapeared_streams:bool = True,
                 clean:bool = False):
    """
    Écrit toutes les configurations dans les répertoires corrects.
    Si :clean est True, alors on supprime les fichiers non utilisés.
    """
    slclients_to_restart = []
    filter_files_to_clean : List[Path] = []
    if clean and len(filters) != 0 :
        filters_dir = Path(filters[0].confdir)
        filter_files_to_clean = sorted(f for f in filters_dir.iterdir() if f.is_file() and f.match("*__[0-9][0-9]"))
    for f in filters:
        # On regarde si un fichier de filtre existe déjà
        # Si oui, on le charge et on le compare au filtre existant
        # Si ils sont identiques, on passe
        # Si ils diffèrent, on l'écrit et on notifie supervisor
        previous_filter = Filter(code=f.code, rtserver=f.rtserver, chunk=f.chunk, confdir=f.confdir, read_from_file=True)

        if previous_filter != f:
            # TODO: Prevent deletion of missing streams
            if keep_disapeared_streams:
                for stream in [ s for s in previous_filter.streams if s not in f.streams  ]:
                    f.add_stream(stream)
            # Print a diff of the streams to the logs
            logger.info("Differences in selection file %s:\n%s",
                        f.filename(),
                        "\n".join(list(
                            difflib.unified_diff(
                                [str(s) for s in previous_filter.selections],
                                [str(s) for s in f.selections]))))
            f.write_file()
            slclients_to_restart.append(f.slclient_id())
        else:
            logger.debug(f"File {f.filename()} is already OK")
        # S'il faut faire le ménage, alors on ne doit pas supprimer ce fichier.
        if clean:
            try:
                filter_files_to_clean.remove(Path(f.filename()))
            except ValueError:
                # On s'en fiche
                pass

    if restart_processes:
        for s in slclients_to_restart:
            logger.info(f"Restarting process {s}")
            subprocess.run(['supervisorctl', 'restart', s])

    if clean:
        logger.info("Deleting unused filter files : %s", filter_files_to_clean)
        for f in filter_files_to_clean:
            f.unlink()

def write_supervisord_conf(filters: List[Filter],
                           confpath: str,
                           mode: str,
                           archivedir: str = ""):
    """
    archive_dir, s'il est positionné doit pointer sur un chemin vers lequel écrire l'archive SDS.
    """
    logger.info("Writing new supervisord configuration")
    archive_opt=""
    if mode == "budcollector":
        archive_opt=f"-f 300 -SDS {archivedir}"
    supervisord_conf = ""
    for f in filters:
        # Portion de configuration supervisord pour ce filtre
        if mode == "rtserver":
            archive_opt=f"-A {archivedir}/%%Y/{f.rtserver}/{f.code}/%%Y.%%j.%%H/{f.chunk:02d}"

        supervisord_conf += f"""
[program:{f.slclient_id()}]
command=/usr/local/bin/slarchive -l {f.filename()} {archive_opt} -x {f.filename()}.state:600 {f.rtserver}
autorestart=true
stdout_syslog=true
stderr_syslog=true
"""
    # On écrit toute la configuration supervisord
    with open(confpath , 'w') as fp:
        fp.write(supervisord_conf)
    logger.info("Supervisord: reloading configurations")
    subprocess.run(['supervisorctl', 'update'])
