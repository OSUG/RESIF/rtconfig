#!/bin/bash

usage(){
    echo "Update real time data collectors configuration and restart them if needed using supervisor"
    echo "Usage: update-stream.bash [-n] ROLE"
    echo "ROLE is either rtserve or budcollector"
}

make_filter_files(){
    # Cette fonction génère les fichiers filtres utilisés par les process de collecte slinktool.
    # Un fichier filtre contient un NSLC par ligne.
    # Pour le constituer, on liste tous les channels d'un réseau à partir des métadonnées
    #
    # Parametres:
    # destdir: répertoire de destination
    # net : le réseau sismo
    # filter: optionnel, on peut définir un filtre sur les canaux
    destdir=$1
    net=$2
    filter=$3
    channels_command="xmlstarlet sel -N x='http://www.fdsn.org/xml/station/1' -T -t -m \"/x:FDSNStationXML/x:Network[@code='$net']/x:Station/x:Channel\" -v ../../@code -o ' ' -v ../@code -o ' ' -v @code -n | sort -u -"
    # Si un filtre a été fourni, alors on l'applique sur la liste.
    if [ "$3" ]; then
       channels_command="$channels_command | egrep -e '$filter'"
    fi
    channels_list=$(echo $stationxml_channel | eval "$channels_command")
    # Si on est un gros réseau :
    # Crée des paquets de channels sous forme de fichiers nommés NET_XX au format de filtres slinktool
    if [[ $(echo "$channels_list"|wc -l) -gt $channel_limit ]]; then
        echo "$channels_list" | split -l $channel_limit -d - $destdir/${net}_
    else
        # Sinon, on peut juste créer un filtre sur le nom du réseau
        echo "$channels_list" > $destdir/$net
    fi
}

make_rt_config(){
    SERVICE=$1
    dryrun=$2
    echo "==== Generation de la config pour $SERVICE ===="

    # Création d'un répertoire temporaire pour générer la config
    SERVICE_DIR=$(mktemp -d /tmp/rtconfig_XXXX)
    SUPERVISORD_FILE=$SERVICE_DIR/slclient_processes.ini
    SUPERVISORD_CONFDIR=$(dirname $(awk  '/^files = /{print $3}' $SUPERVISORD_CONF))

    # On parcourt chaque réseau
    for net in $(echo $active_networks); do
        # 1. Déterminer la source de diffusion du réseau
        #    Cela peut être fonction de la 2e lettre du channel (vélocimétrie ou accéléro)
        #    Déterminer également le répertoire de destination pour la donnée collectée
        #      - sds_dest : dossier de destination de l'archive SDS
        #      - rtserver : serveur + port
        echo "  Configuration de $net"
        rtport=18000
        case $SERVICE in
            budcollector)
                # Quand on est budcollector, la destination, c'est toujours :
                sds_dest=/mnt/nfs/summer/bud_data
                # Et le serveur, c'est toujours :
                rtserver=rtserve.resif.fr
                # Si c'est un réseau sous embargo, alors il faut récupérer la donnée sur le port 18001
                if [[ $(echo $stationxml_channel | xmlstarlet sel -N x="http://www.fdsn.org/xml/station/1" -t -v "/x:FDSNStationXML/x:Network[@code='$net']/@restrictedStatus") = "closed" ]]; then
                    rtport=18001
                fi
                if [[ $(slinktool -Q ${rtserver}:${rtport} | egrep -e "^$net" -c) -eq 0 ]]; then
                    echo "  Le réseau $net n'a pas de donnée temps réel sur ${rtserver}:${rtport}. Il est ignoré."
                    continue
                fi
                ;;
            rtserve)
                sds_dest=/home/sysop/work/in/  # cas général
                case $net in
                    WI|PF|GL|MQ|G|QM)
                        # Les réseaux distribués par l'IPGP
                        rtserver=rtserver.ipgp.fr
                        ;;
                    RA)
                        rtserver=bud-resif-a.u-ga.fr
                        ;;
                    FR|ND|MT|CL|FO)
                        # Attention, pour ces réseaux, la donnée est diffusée soit par renass soit par rap
                        rtserver=rtserve.u-strasbg.fr
                        ;;
                    RD)
                        rtserver=132.167.240.22
                        ;;
                    *)
                        # Est ce un réseau temporaire ?
                        if [[ "$net" =~ ^[0-9XYZ][0-9A-Z] ]]; then
                            rtserver=bud-resif-a.u-ga.fr
                            # Vérifions que ce réseau est bien diffusé. Parfois il existe de la métadonnée, mais il n'y a pas de donnée temps réel associée
                            if [[ $(slinktool -Q $rtserver | egrep -e "^$net" -c) -eq 0 ]]; then
                                echo "  Le réseau $net n'a pas de donnée temps réel sur $rtserver. Il est ignoré."
                                continue
                            fi
                            # Est ce un réseau restreint ?
                            if [[ $(echo $stationxml_channel | xmlstarlet sel -N x="http://www.fdsn.org/xml/station/1" -t -v "/x:FDSNStationXML/x:Network[@code='$net']/@restrictedStatus") = "closed" ]]; then
                                sds_dest=/home/sysop/work/out-restricted  # réseau restreint
                            fi
                        else
                            # Sinon, alors on ne sait pas quoi faire, on annule tout
                            echo "  ATTENTION: Réseau $net inconnu. Il faut paramétrer ce script et le relancer"
                            continue
                        fi
                    ;;
                esac
                ;;
        esac
        # 1. Pour chaque réseau, récupérer la liste triée des channels
        # 2. Faire des paquets de channels et un fichier par paquets au format de filtre slinktool
        # 3. Pour chaque paquet, créer une entrée pour supervisor
        declare -A network_sources
        # Les flux FR, ND et RD sont diffusés séparéments. Accéléro par le RAP et Vélo par le RENASS
        if [[ "$net" =~ ^(FR|ND|RD)$ && "$SERVICE" = "rtserve" ]]; then
            echo "    Traitement des canaux non accéléro"
            make_filter_files $SERVICE_DIR $net '.[^N].$'
            for f in $(ls $SERVICE_DIR/$net*); do
                file=$(basename $f)
                network_sources[${file}]="$rtserver:$rtport"
            done
            echo "    Traitement des canaux accélérométriques (.N.)"
            mkdir -p $SERVICE_DIR/accelero
            make_filter_files $SERVICE_DIR/accelero $net '.N.$'
            for f in $(ls $SERVICE_DIR/accelero/$net*); do
                file=$(basename $f)
                cp $f $SERVICE_DIR/${file}_accelero
                network_sources[${file}_accelero]="bud-resif-a.u-ga.fr:18000"
            done
            rm -rf $SERVICE_DIR/accelero
        else
            make_filter_files $SERVICE_DIR $net
            for f in $(ls $SERVICE_DIR/$net*); do
                file=$(basename $f)
                network_sources[${file}]="$rtserver:$rtport"
            done
        fi
        for f in $(ls $SERVICE_DIR/$net*); do
            file=$(basename $f)
            # Déclaration des processus dans supervisord
            case $SERVICE in
                "rtserve")
                    # Si c'est rtserve, alors on crée un fichier par heure et par process slinktool
                    slcommand="$(which slarchive) -l /home/sysop/config/${SERVICE}/${file} -A $sds_dest/%%Y/%%n/${file}/%%H  -x /home/sysop/config/${SERVICE}/${file}.state:600 ${network_sources[${file}]}"
                    ;;
                "budcollector")
                    # Sinon, on fait du SDS avec slarchive
                    slcommand="$(which slarchive) -l /home/sysop/config/${SERVICE}/${file} -f $(( $channel_limit * 2 )) -SDS $sds_dest -x /home/sysop/config/${SERVICE}/${file}.state:600 ${network_sources[${file}]}"
                    ;;
            esac

            echo "[program:slclient_${file}]
command=${slcommand}
autorestart=true
stdout_syslog=true
stderr_syslog=true" >> $SUPERVISORD_FILE
        done
    done
    # Adding blank line at end of config
    echo >> $SUPERVISORD_FILE
    echo "### Generated at $(date '+%Y-%m-%d %H:%M') by $USER@$(hostname)" >> $SUPERVISORD_FILE

    echo "  Copying configuration files (Option dryrun: $dryrun)"
    if [[ $dryrun -eq 0 ]]; then
        files_updated=$(rsync -avpci --delete --exclude '*.ini' $SERVICE_DIR/ /home/sysop/config/${SERVICE}/)
        echo "$files_updated"
        files_updated=$(echo "$files_updated" | awk '/^>f......... /{print $2}')
        echo $files_updated
        cp $SUPERVISORD_FILE $SUPERVISORD_CONFDIR
        supervisorctl -c $SUPERVISORD_CONF update || echo "Attention, un problème est survenu au chargement de la config supervisord."
        for f in $files_updated; do
            echo "Redémarrage de slclient pour $f qui a été modifié"
            supervisorctl -c $SUPERVISORD_CONF restart slclient_${f} || echo "Attention, un problème est survenu au redémarrage de slinktool pour $f"
        done
    fi
    rm -rf "$SERVICE_DIR"
}

echo "$(date '+%Y-%m-%d %X') Starting"
dryrun=0
while getopts "r:nh" o; do
    case ${o} in
        r)
            role=$OPTARG
            ;;
        n)
            dryrun=1
            ;;
        *) usage
           exit 0
           ;;
    esac
done

# on vérifie que le nom du rôle est valide
case $role in
    budcollector)
        echo "Generating configurations for $role"
        ;;
    rtserve)
        echo "Generating configurations for $role"
        ;;
    *)
        echo "Missing command argument budcollector or rtserve"
        exit 1
esac

# Vérification de la présence des dépendances
[ $(which supervisorctl) ] || (echo "supervisorctl executable not found"; exit 1)
[ $(which rsync) ] || (echo "rsync executable not found"; exit 1)
[ $(which slinktool) ] || (echo "slinktool executable not found"; exit 1)
[ $(which slarchive) ] || (echo "slarchive executable not found"; exit 1)

channel_limit=150
# Get all current channels as of today
stationxml_channel=$(wget -qO- "http://ws.resif.fr/fdsnws/station/1/query?refreshcache&level=channel&endafter=$(date '+%Y-%m-%d')")
if [[ $? -ne 0 ]]; then
    echo "Error getting metadata. Exiting"
    exit 1
fi
stationxml_channel=$(echo $stationxml_channel | iconv -f ISO_8859-1 -t utf-8)
if [[ $? -ne 0 ]]; then
    echo "Error in metadata encoding. Exiting"
    exit 1
fi

active_networks=$(echo $stationxml_channel | xmlstarlet sel -N x="http://www.fdsn.org/xml/station/1" -t -v "/x:FDSNStationXML/x:Network/@code")
if [[ $? -ne 0 ]]; then
    echo "Error parsing metadata XML. Exiting"
    exit 1
fi

if [[ -z $SUPERVISORD_CONF ]]; then
    SUPERVISORD_CONF=/etc/supervisor/supervisord.conf
fi

echo "Generating configurations for $role"
make_rt_config $role $dryrun
echo "$(date '+%Y-%m-%d %X') Done"
