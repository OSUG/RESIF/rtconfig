#!/usr/bin/env python3
from rtconfig.filter import Filter,Stream
from pathlib import Path
import tempfile

def test_new_filter():
    slclientfilter = Filter(code="RA", rtserver="bud-resif-a.u-ga.fr")
    slclientfilter.add_stream(Stream(net="FR", sta="CIEL", loc="00", cha="HNZ"))
    slclientfilter.add_stream(Stream(net="FR", sta="CIEL", loc="00", cha="HNE"))
    assert isinstance(slclientfilter, Filter)
    print(slclientfilter)

def test_format_streams():
    slclientfilter = Filter(code="RA", rtserver="bud-resif-a.u-ga.fr")
    slclientfilter.add_stream(Stream(net="FR", sta="CIEL", loc="00", cha="HNZ"))
    slclientfilter.add_stream(Stream(net="FR", sta="CIEL", loc="00", cha="HNE"))
    print(slclientfilter)
    slclientfilter.write_file()

def test_slclient_id():
    slclientfilter = Filter(code="RA", rtserver="bud-resif-a.u-ga.fr")
    assert slclientfilter.slclient_id() == "bud_resif_a_u_ga_fr__RA__00"

def test_format_stream():
    slclientfilter = Filter(code="RA", rtserver="bud-resif-a.u-ga.fr")
    stream = Stream()
    stream.parse("XP_FR23A_00_HHN")
    assert slclientfilter.format_stream(stream) == "XP FR23A 00HHN"

def test_add_stream():
    slclientfilter = Filter(code="RA", rtserver="bud-resif-a.u-ga.fr")
    stream = Stream()
    stream.parse("XP_FR23A_00_HHN")
    slclientfilter.add_stream(stream)
    assert slclientfilter.format_stream(stream) in slclientfilter.selections

def test_conf_path():
    slclientfilter = Filter(code="RA", rtserver="bud-resif-a.u-ga.fr")
    assert slclientfilter.filename() == Path("bud_resif_a_u_ga_fr__RA__00")

def test_from_stream():
    slclientfilter = Filter(code="FO", rtserver="rtserve.u-strasbg.fr", confdir="tests")
    slclientfilter._streams_from_file()
    assert "FO OPS 00LHZ" in slclientfilter.selections

def test_write_file():
    with tempfile.TemporaryDirectory() as tempdir:
        slclientfilter = Filter(code="FR", rtserver="rtserve.u-strasbg.fr", confdir=tempdir)
        stream = Stream(net="FR", sta="CIEL", loc="00", cha="HNZ")
        slclientfilter.add_stream(stream)
        slclientfilter.write_file()
        with open(slclientfilter.filename(),'r' ) as fp:
            streams_lines = [ s.strip() for s in fp.readlines() ]
            assert slclientfilter.format_stream(stream) in streams_lines
