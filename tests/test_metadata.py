#!/usr/bin/env python3


import rtconfig.metadata as metadata
import logging


def test_load_rule():
    with open("tests/config.yaml", 'r') as fp:
        metadata.inventory.load_rules(fp)

def test_streams():
    streams = metadata.inventory.get_active_streams()
    assert len(streams) > 0
