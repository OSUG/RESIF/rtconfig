#!/usr/bin/env python3

from rtconfig.network import Network
from rtconfig.streams import Stream


def test_network_add_stream():
    net = Network(code="FR", restricted=False, exclude_accelero=True)
    # Try to add accelerometer (should be refused)
    stream = Stream(net="FR", sta="CIEL", loc="00", cha="HNZ")
    assert net.add_stream(stream)
    # Try to add other channel
    stream = Stream(net="FR", sta="CIEL", loc="00", cha="HHZ")
    net.add_stream(stream)
    assert stream in net.streams

    # Try to add channel not from the same network
    assert not net.add_stream(Stream(net="RA", sta="OGH1", loc="00", cha="HNZ"))
