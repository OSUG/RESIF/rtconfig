#!/usr/bin/env python3

from rtconfig.node import Node

def test_node():
    # Test d'un nœud public
    node = Node(host='rtserve.resif.fr')
    assert(len(node.networks) > 0)

def test_unresponsive_node():
    # Test d'un nœud public
    with pytest.raises(RuntimeError):
        node = Node(host='rtserve.u-strasbg.fr')
