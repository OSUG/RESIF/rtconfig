#!/usr/bin/env python3
import pytest
from rtconfig.rule import RoutingRule

def test_routing_intit():
    with pytest.raises(ValueError):
        r = RoutingRule(networks_regex="(FR|ND|FO|RD", channels_regex=".N.", host='bud-resif-a.u-ga.fr')


def test_routing_rule():
    r = RoutingRule(networks_regex="(FR|ND|FO|RD)", channels_regex=".N.", host='bud-resif-a.u-ga.fr')
    assert r.validate(net="FR", cha="HNN") == True
    assert r.validate(net="G", cha="HNN") == False
    assert r.validate(net="FO", cha="HHN") == False
    assert r.validate(net="FO", cha="HNN") == True

    r = RoutingRule(networks_regex="(FR|ND|FO|RD)", channels_regex=".[^N].", host='bud-resif-a.u-ga.fr')
    assert r.validate(net="FR", cha="HNN") == False
    assert r.validate(net="G", cha="HNN") == False
    assert r.validate(net="FO", cha="HHN") == True
    assert r.validate(net="FO", cha="HNN") == False
