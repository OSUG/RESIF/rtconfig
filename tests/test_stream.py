#!/usr/bin/env python3

import pytest
from rtconfig.streams import Stream


def test_stream_parse_ok():
    stream = Stream()
    stream.parse("XP_FR23A_00_HHN")
    assert stream.net == "XP"
    assert stream.sta == "FR23A"
    assert stream.loc == "00"
    assert stream.cha == "HHN"

    stream.parse("RD_FR23A__HHN")
    assert stream.net == "RD"
    assert stream.sta == "FR23A"
    assert stream.cha == "HHN"

    stream.parse("RD FR23A 00 HHN")
    assert stream.net == "RD"
    assert stream.sta == "FR23A"
    assert stream.cha == "HHN"

    stream.parse("FR ADHE 00 HNE")
    assert stream.net == "FR"
    assert stream.sta == "ADHE"
    assert stream.cha == "HNE"
    assert stream.loc == "00"

    stream.parse("FR ADHE  HNE")
    assert stream.net == "FR"
    assert stream.sta == "ADHE"
    assert stream.cha == "HNE"
    assert stream.loc == ""

    stream.parse("FR.ADHE..HNE")
    assert stream.net == "FR"
    assert stream.sta == "ADHE"
    assert stream.cha == "HNE"
    assert stream.loc == ""

    stream.parse("RD RJF      BHZ D 2023-05-01 04:44:30  -  2023-05-01 11:26:09")
    assert stream.net == "RD"
    assert stream.sta == "RJF"
    assert stream.loc == ""
    assert stream.cha == "BHZ"

    stream.parse("RD ROSF     BHE D 2023-05-01 04:44:50  -  2023-05-01 11:26:29")
    assert stream.net == "RD"
    assert stream.sta == "ROSF"
    assert stream.loc == ""
    assert stream.cha == "BHE"

def test_stream_parse_fail():
    stream = Stream()
    with pytest.raises(ValueError):
       stream.parse("XP_FR23A00_HHN")

def test_sorted():
    stream1 = Stream()
    stream2 = Stream()
    stream1.parse("XP_FR23A_00_HHN")
    stream2.parse("XP_FR23B_00_HHN")
    assert sorted([stream1,stream2]) == [stream1,stream2]
    assert sorted([stream2,stream1]) == [stream1,stream2]
